import json
import logging
import sys

import greengrasssdk

# Logging
logger = logging.getLogger(__name__)
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG)

# SDK Client
client = greengrasssdk.client("iot-data")

max_co2 = {}

def lambda_handler(event, context):
    global max_co2

    if 'car' in event:
        if event['car'] not in max_co2 or event['CO2'] > max_co2[event['car']]:
            max_co2[event['car']] = event['CO2']


        #TODO3: Return the result
        if (event['CO2']) == -1:
            client.publish(
                topic="car_topic/{}".format(event['car']),
                payload=json.dumps(
                    {"maxCO2": max_co2[event['car']]}
                ),
            )

    return
